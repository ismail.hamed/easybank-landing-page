const nav = document.querySelector(".navigation");
const navToggle = document.querySelector(".mobile-nav-toggle");
const overlay = document.querySelector(".overlay");
navToggle.addEventListener("click", () => {
  const visiblity = nav.getAttribute("data-visible");
  if (visiblity === "false") {
    openNav();
  } else {
    closeNav();
  }
});

window.addEventListener("resize", closeNav);

function closeNav() {
  nav.setAttribute("data-visible", false);
  navToggle.setAttribute("aria-expanded", false);
  document.body.classList.remove("noscroll");
  overlay.style.display = "none";
}
function openNav() {
  nav.setAttribute("data-visible", true);
  navToggle.setAttribute("aria-expanded", true);
  document.body.classList.add("noscroll");
  overlay.style.display = "block";
}
